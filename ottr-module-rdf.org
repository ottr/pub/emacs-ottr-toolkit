#+title: docTTR template package "rdf"
#+author: Martin G. Skjæveland (templates), Johan W. Klüwer (tests)
#+email: m.g.skjaeveland@gmail.com, johan.wilhelm.kluewer@dnv.com
#+date: 2020-08-21
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: H:4

* What is this
This file is a demonstration of how org-mode can provide an authoring
environment for OTTR templates. It uses the /rdf/ templates
(http://tpl.ottr.xyz/rdf/) as examples.

* Template definitions
:PROPERTIES:
:header-args:stottr:  :noweb yes :tangle ./stottr/rdf.stottr
:header-args:ttl:  :noweb yes
:header-args: :padline yes
:END:

** Prefixes
#+name: stottr-prefixes
#+begin_src stottr :noweb yes
  @prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
  @prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
  @prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
  @prefix ottr:  <http://ns.ottr.xyz/0.4/> .
  @prefix o-rdf: <http://tpl.ottr.xyz/rdf/0.1/> .
  @prefix o-docttr: <http://tpl.ottr.xyz/p/docttr/0.1/> .

  @prefix ex: <http://example.org/> .
#+end_src

** Private templates
** Public templates

*** o-rdf:Type
The template:
#+name: Type
#+begin_src stottr :var annotations=(ottr-annotations-str "o-rdf:Type" "annotations-Type")
o-rdf:Type [ 
    ottr:IRI ?resource, 
    ottr:IRI ?class
 ]
#annotations
:: {
ottr:Triple(?resource, rdf:type, ?class)
} .
#+end_src

#+name: annotations-Type
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

A table of test data:
# Execute the call with C-c C-c to insert blank table for template.
#+name: Type-data-table
#+call: template-data-table( template="Type" ) :cache yes

#+RESULTS[ebb5da10fd21e1c605cb8518a6728d8e6b1ab1ee]: Type-data-table
| resource   | class     |
|------------+-----------|
| ex:Lucille | ex:Guitar |
| ex:Lucille | ex:Black  |


A bOTTR map to connect the template to the table of test data:
# Execute the call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=Type-data-table, file="Type-data-table.csv", template="Type" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/Type_Type-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select resource, class
from csvread( './data/Type-data-table.csv' )
""" ;
ottr:template o-rdf:Type ;
ottr:argumentMaps (
  [ ottr:type ottr:IRI ] #  ?resource
  [ ottr:type ottr:IRI ] #  ?class
) .
#+end_src

The result of lifting:
# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=Type-data-table, file="Type-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/Type_Type-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .

ex:Lucille  a   ex:Black , ex:Guitar .

#+end_SRC

*** o-rdf:Statement
#+name: Statement
#+begin_src stottr :var annotations=(ottr-annotations-str "o-rdf:Statement" "annotations-Statement")
o-rdf:Statement [ 
    ottr:IRI ?statement,
    ottr:IRI ?subject,
    ! ottr:IRI ?predicate,
    rdfs:Resource ?object
 ]
#annotations
:: {
    o-rdf:Type(?statement, rdf:Statement) ,
    ottr:Triple(?statement, rdf:subject, ?subject) ,
    ottr:Triple(?statement, rdf:predicate, ?predicate) ,
    ottr:Triple(?statement, rdf:object, ?object)
} .
#+end_src

#+name: annotations-Statement
 - Provenance
     1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

# Execute this call with C-c C-c to insert blank table for template.
#+name: Statement-data-table
#+call: template-data-table( template="Statement" ) :cache yes

#+RESULTS[feb0d1eff5c5ef6ec1e738d3a5cd81b28935b797]: Statement-data-table
| statement        | subject   | predicate | object                      |
|------------------+-----------+-----------+-----------------------------|
| ex:myObservation | ex:Popeye | ex:eats   | ex:Spinach                  |
| ex:myHope        | ex:Popeye | ex:saves  | <http://example.org/Olivia> |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=Statement-data-table, file="Statement-data-table.csv", template="Statement" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/Statement_Statement-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select statement, subject, predicate, object
from csvread( './data/Statement-data-table.csv' )
""" ;
ottr:template o-rdf:Statement ;
ottr:argumentMaps (
  [ ottr:type ottr:IRI ] #  ?statement
  [ ottr:type ottr:IRI ] #  ?subject
  [ ottr:type ottr:IRI ] #  ?predicate non-blank
  [ ottr:type rdfs:Resource ] #  ?object
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=Statement-data-table, file="Statement-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/Statement_Statement-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .

ex:myHope  a           rdf:Statement ;
        rdf:object     "<http://example.org/Olivia>" ;
        rdf:predicate  ex:saves ;
        rdf:subject    ex:Popeye .

ex:myObservation  a    rdf:Statement ;
        rdf:object     "ex:Spinach" ;
        rdf:predicate  ex:eats ;
        rdf:subject    ex:Popeye .


[WARNING] Unknown literal datatype rdfs:Resource, defaulting to rdfs:Literal
 >>> at [1: 1] (xyz.ottr.lutra.model.Instance) ottr:Triple(http://example.org/myObservation : LUB<ottr:IRI> ...
#+end_SRC

*** o-rdf:StatementTriple
#+name: StatementTriple
#+begin_src stottr :var annotations=(ottr-annotations-str "o-rdf:StatementTriple" "annotations-StatementTriple")
o-rdf:StatementTriple [ 
    ottr:IRI ?statement,
    ottr:IRI ?subject,
    ! ottr:IRI ?predicate,
    rdfs:Resource ?object
 ]
#annotations
:: {
    o-rdf:Statement(?statement, ?subject, ?predicate, ?object),
    ottr:Triple(?subject, ?predicate, ?object)
} .
#+end_src

#+name: annotations-StatementTriple
 - Provenance
     1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

# Execute this call with C-c C-c to insert blank table for template.
#+name: StatementTriple-data-table
#+call: template-data-table( template="StatementTriple" ) :cache yes

#+RESULTS[f365023821050596a0794bcac3ba66bbd47ea837]: StatementTriple-data-table
| statement        | subject   | predicate | object     |
|------------------+-----------+-----------+------------|
| ex:myObservation | ex:Popeye | ex:eats   | ex:Spinach |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=StatementTriple-data-table, file="StatementTriple-data-table.csv", template="StatementTriple" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/StatementTriple_StatementTriple-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select statement, subject, predicate, object
from csvread( './data/StatementTriple-data-table.csv' )
""" ;
ottr:template o-rdf:StatementTriple ;
ottr:argumentMaps (
  [ ottr:type ottr:IRI ] #  ?statement
  [ ottr:type ottr:IRI ] #  ?subject
  [ ottr:type ottr:IRI ] #  ?predicate non-blank
  [ ottr:type rdfs:Resource ] #  ?object
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=StatementTriple-data-table, file="StatementTriple-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/StatementTriple_StatementTriple-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .

ex:myObservation  a    rdf:Statement ;
        rdf:object     "ex:Spinach" ;
        rdf:predicate  ex:eats ;
        rdf:subject    ex:Popeye .

ex:Popeye  ex:eats  "ex:Spinach" .


[WARNING] Unknown literal datatype rdfs:Resource, defaulting to rdfs:Literal
 >>> at [1: 1] (xyz.ottr.lutra.model.Instance) ottr:Triple(http://example.org/myObservation : LUB<ottr:IRI> ...
#+end_SRC

* docTTR HTML pages                                                :noexport:
# Execute this call with C-c C-c to generate docTTR HTML pages:
#+call: write-docttr()

#+RESULTS:
: Wrote http://tpl.ottr.xyz/rdf/0.1/Statement to docttr\tpl.ottr.xyz\rdf\0.1\Statement.html
: Wrote http://tpl.ottr.xyz/rdf/0.1/Type to docttr\tpl.ottr.xyz\rdf\0.1\Type.html
: Wrote http://tpl.ottr.xyz/rdf/0.1/StatementTriple to docttr\tpl.ottr.xyz\rdf\0.1\StatementTriple.html
: Wrote index files to docttr
: Wrote index files to docttr\tpl.ottr.xyz
: Wrote index files to docttr\tpl.ottr.xyz\rdf
: Wrote index files to docttr\tpl.ottr.xyz\rdf\0.1

* Startup for this file                                            :noexport:
#+STARTUP: hideblocks
# Local Variables:
# mode: org
# org-confirm-babel-evaluate: nil
# org-babel-default-inline-header-args: ((:exports . "code"))
# org-latex-listings: t
# stottr-dir: "./stottr"
# bottr-dir: "./bottr"
# docttr-dir: "./docttr"
# data-dir: "./data"
# eval: (org-babel-load-file (concat emacs-ottr-toolkit-root "ottr-extra.org"))
# eval: (org-babel-lob-ingest (concat emacs-ottr-toolkit-root "ottr-lob.org"))
# eval: (setq-local org-link-abbrev-alist
# (quote
#  (
# ( "owl" . "http://www.w3.org/2002/07/owl#")
# ( "rdf" . "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
# ( "xml" . "http://www.w3.org/XML/1998/namespace")
# ( "xsd" . "http://www.w3.org/2001/XMLSchema#")
# ( "rdfs" . "http://www.w3.org/2000/01/rdf-schema#")
# ( "dc" . "http://purl.org/dc/elements/1.1/")
# ( "skos" . "http://www.w3.org/2004/02/skos/core#")
# ( "foaf" . "http://xmlns.com/foaf/0.1/")
# ( "ottr" . "http://ns.ottr.xyz/0.4/")
# ( "o-rdf" . "http://tpl.ottr.xyz/rdf/0.1/")
# ( "o-rdfs" . "http://tpl.ottr.xyz/rdfs/0.1/")
# ( "o-owl-ax" . "http://tpl.ottr.xyz/owl/axiom/0.1/")
# ( "o-docttr" . "http://tpl.ottr.xyz/p/docttr/0.1/")
# )))
# org-todo-keyword-faces: (("REVIEW" . "orange") ("REDO" . "orange") ("N/A" . "darkgreen"))
# org-log-into-drawer: t
# eval: (setq-local org-babel-default-header-args:stottr
# '((:comments . "link")
#   ))
# End:
