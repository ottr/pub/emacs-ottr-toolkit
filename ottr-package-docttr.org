#+title: docTTR templates for documenting OTTR templates
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup


* What is this
This file is a demonstration of how org-mode can provide an authoring
environment for OTTR templates. It uses the docTTR templates
(http://tpl.ottr.xyz/p/docttr/) as examples.

To provide a complete example, this file contains the templates for
documenting templates, as found at [[https://drive.google.com/file/d/1Mgyq5g3NFSXFXyOJdEbl3V8uMm-_yun8/view][p. 8 of "The Core OTTR Template Library"]]:
#+begin_quote
 - Signature: label, description, scope and editorial notes, related resources
 - Provenance: time of creation and update, authors and contributors
 - Version: status, version number, references to previous and next versions
 - ChangeNote: change description, including author and timestamp of the change
 - Example: explanatory examples of the template
 - Deprecated: mark the template as deprecated, including an explanation of why
 - Parameter: parameter description, example, notes
#+end_quote

* Template definitions
	:PROPERTIES:
	:header-args:stottr: :tangle ./stottr/docttr-test.stottr :noweb yes
	:header-args:ttl:  :noweb yes
    :header-args: :padline yes
	:END:

** Prefixes
#+name: stottr-prefixes
#+begin_src stottr
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix ottr:  <http://ns.ottr.xyz/0.4/> .
@prefix ex:  <http://example.ottr.xyz/> .

@prefix pav:   <http://purl.org/pav/> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix dc:    <http://purl.org/dc/elements/1.1/> .

@prefix o-rdf: <http://tpl.ottr.xyz/rdf/0.1/> .
@prefix o-rdfs: <http://tpl.ottr.xyz/rdfs/0.2/> .
@prefix o-docttr: <http://tpl.ottr.xyz/p/docttr/0.1/> .
#+end_src
** Private templates
*** o-docttr:SaysWho
#+begin_src stottr
o-docttr:SaysWho[
    ottr:IRI ?statement,         ## the statement
    xsd:dateTime ?when,          ## when did they say it
    NEList<rdfs:Resource> ?who,  ## who said it
    ? List<xsd:string> ?why      ## why did they say it
] :: {
    ottr:Triple(?statement, pav:createdOn, ?when),
    cross | ottr:Triple(?statement, pav:createdBy, ++?who),
    cross | ottr:Triple(?statement, skos:note, ++?why)
} .
#+end_src
*** o-docttr:SaysWhoTriple
#+begin_src stottr
o-docttr:SaysWhoTriple[
    ottr:IRI ?statement,
    ottr:IRI ?subject,
    ! ottr:IRI ?predicate,
    rdfs:Resource ?object,
    xsd:dateTime ?when,          ## when did they say it
    NEList<rdfs:Resource> ?who,  ## who said it
    ? List<xsd:string> ?why       ## why did they say it
] :: {
    o-rdf:StatementTriple(?statement, ?subject, ?predicate, ?object),
    o-docttr:SaysWho(?statement, ?when, ?who, ?why)
} .
#+end_src
** Public templates
*** o-docttr:ChangeNote
#+begin_src stottr :var annotations=(ottr-annotations-str "o-docttr:ChangeNote" "annotations-ChangeNote")
o-docttr:ChangeNote[
    ottr:IRI ?resource,          ## the resource to append a note
    xsd:string ?note,            ## the change note
    xsd:dateTime ?when,          ## when did they say it
    NEList<rdfs:Resource> ?who,  ## who said it
    ? List<xsd:string> ?why      ## why did they say it
]
#annotations
:: {
    o-docttr:SaysWhoTriple([], ?resource, skos:changeNote, ?note, ?when, ?who, ?why)
} .
#+end_src

#+name: annotations-ChangeNote
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Provide change notes for any resource."
   3. ?scope =?! string=  :: "Use to explain changes to resources, including provenance data for the note"
   4. ?notes =? NEList<string>=  :: 
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! ottr:IRI= :: ottr:draft
   2. ?version =! xsd:string= :: "0.1.1"
   3. ?previousVersion =!? ottr:IRI= :: 
   4. ?nextVersion =!? ottr:IRI= :: 

*** o-docttr:Example
#+begin_src stottr  :var annotations=(ottr-annotations-str "o-docttr:Example" "annotations-Example")
o-docttr:Example[
    ottr:IRI ?resource,          ## the resource to exemplify
    xsd:string ?example,         ## the example
    xsd:dateTime ?when,          ## when did they say it
    NEList<rdfs:Resource> ?who,  ## who said it
    ? List<xsd:string> ?why      ## why did they say it
]
#annotations
:: {
    o-docttr:SaysWhoTriple([], ?resource, skos:example, ?example, ?when, ?who, ?why)
} .
#+end_src

#+name: annotations-Example
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Provide examples for any resource."
   3. ?scope =?! string=  :: "Use to explain resources by giving examples, including provenance data for the example"
   4. ?notes =? NEList<string>=  :: 
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! ottr:IRI= :: ottr:draft
   2. ?version =! xsd:string= :: "0.1.1"
   3. ?previousVersion =!? ottr:IRI= :: 
   4. ?nextVersion =!? ottr:IRI= :: 
*** o-docttr:Deprecated
#+begin_src stottr :var annotations=(ottr-annotations-str "o-docttr:Deprecated" "annotations-Deprecated")
o-docttr:Deprecated [
    ! ottr:IRI ?resource,          ## the deprecated
    ! xsd:string ?explanation,     ## why is it deprecated
    ? List<ottr:IRI> ?seeAlso,     ## other relevant resources, e.g., reasons for deprecating
    ! xsd:dateTime ?when,          ## when was the template deprecated
    NEList<rdfs:Resource> ?who,    ## who deprecated the template
    ? List<xsd:string> ?why        ## any additional notes for why the template was deprecated
]
#annotations
:: {
    o-docttr:SaysWhoTriple(_:statement, ?resource, owl:deprecated, true, ?when, ?who, ?why),
    ottr:Triple(_:statement, skos:changeNote, ?explanation),
    cross | ottr:Triple(_:statement, rdfs:seeAlso, ++?seeAlso)
} .
#+end_src

#+name: annotations-Deprecated
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Use to indicate that a resource,
      e.g., template or OWL construct, is deprecated, providing an
      explanation for why, including provenance data. The explanation
      and provenance data is attached to a reified representation of
      the deprecation statement."  
   3. ?scope =?! string=  :: 
   4. ?notes =? NEList<string>=  :: 
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! ottr:IRI= :: ottr:draft
   2. ?version =! xsd:string= :: "0.1.1"
   3. ?previousVersion =!? ottr:IRI= :: 
   4. ?nextVersion =!? ottr:IRI= :: 
*** o-docttr:Parameter
#+begin_src stottr :var annotations=(ottr-annotations-str "o-docttr:Parameter" "annotations-Parameter")
o-docttr:Parameter [
    ! ottr:IRI ?resource,
    ! xsd:string ?description,
    !? xsd:string ?example,
    !? xsd:string ?note
]
#annotations
:: {
    ottr:Triple(?resource, dc:description, ?description),
    ottr:Triple(?resource, skos:example, ?example),
    ottr:Triple(?resource, skos:note, ?note)
} .
#+end_src

#+name: annotations-Parameter
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Use for documenting each parameter of
      a published template, providing descriptions and examples." 
   3. ?scope =?! string=  :: 
   4. ?notes =? NEList<string>=  :: "The template should be used in
      combination with other templates in the same package."
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: "Use of this template
      depends on parameters represented by IRIs, which they are
      usually not, but we expect this to change soon." 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! ottr:IRI= :: ottr:draft
   2. ?version =! xsd:string= :: "0.1.1"
   3. ?previousVersion =!? ottr:IRI= :: 
   4. ?nextVersion =!? ottr:IRI= :: 
*** o-docttr:Signature
#+begin_src stottr  :var annotations=(ottr-annotations-str "o-docttr:Signature" "annotations-Signature")
o-docttr:Signature[
    ottr:IRI ?resource,
    ? xsd:string ?label,
    ! xsd:string ?description,
    ?! xsd:string ?scope,
    ? NEList<xsd:string> ?notes,
    ? NEList<ottr:IRI> ?seeAlso,
    ? NEList<xsd:string> ?editorialNote
]
#annotations
:: {
    ottr:Triple(?resource, rdfs:label, ?label)  ,
    ottr:Triple(?resource, dc:title, ?label),
    ottr:Triple(?resource, dc:description, ?description),
    ottr:Triple(?resource, skos:scopeNote, ?scope),
    cross | ottr:Triple(?resource, skos:note, ++?notes),
    cross | ottr:Triple(?resource, rdfs:seeAlso, ++?seeAlso),
    cross | ottr:Triple(?resource, skos:editorialNote, ++?editorialNote)
} .
#+end_src
#+name: annotations-Signature
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Use to document a published template,
      providing descriptions and usage notes." 
   3. ?scope =?! string=  :: 
   4. ?notes =? NEList<string>=  :: "The template should be used in
      combination with other templates in the same package." 
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: "Suggest to refactor,
      keeping the signature, but put body in a more generic template
      as this template could be used for documenting any
      resource. Align refactoring with a possible refactoring of
      o-docttr:Parameter." 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! ottr:IRI= :: ottr:draft
   2. ?version =! xsd:string= :: "0.1.1"
   3. ?previousVersion =!? ottr:IRI= :: 
   4. ?nextVersion =!? ottr:IRI= :: 
 - Example
   1. ?example =string= :: "The template uses an instance of itself to document itself"
   2. ?when =dateTime= :: 2020-08-21T00:00:00Z
   3. ?who =NEList<Resource>= :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?why =? List<string>= ::  
*** o-docttr:Version
#+begin_src stottr :var annotations=(ottr-annotations-str "o-docttr:Version" "annotations-Version")
o-docttr:Version[
    ottr:IRI ?resource,
    ! ottr:IRI ?status = ottr:incomplete,
    ! xsd:string ?version,
    !? ottr:IRI ?previousVersion,
    !? ottr:IRI ?nextVersion
]
#annotations
:: {
    ottr:Triple(?resource, ottr:status, ?status),
    ottr:Triple(?resource, owl:versionInfo, ?version),
    ottr:Triple(?resource, pav:hasPreviousVersion, ?previousVersion),
    ottr:Triple(?nextVersion, pav:hasPreviousVersion, ?resource)
} .
#+end_src

#+name: annotations-Version
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Use to specify the current and
      related version of a template. The '?version' parameter expects
      a string version number using the format [major.minor.patch]
      (see https://semver.org/), while the parameters
      '?previousVersion' and '?nextVersion' expects a resolving
      template IRI." 
   3. ?scope =?! string=  :: 
   4. ?notes =? NEList<string>=  :: "The template should be used in
      combination with other templates in the same package."
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:, <https://semver.org/>
   6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! IRI= :: ottr:draft
   2. ?version =! string= :: "0.1.1"
   3. ?previousVersion =!? IRI= :: 
   4. ?nextVersion =!? IRI= :: 
*** o-docttr:Provenance
#+begin_src stottr :var annotations=(ottr-annotations-str "o-docttr:Provenance" "annotations-Provenance")
o-docttr:Provenance[
    ottr:IRI ?resource,
    ! xsd:dateTime ?created,
    ?! xsd:dateTime ?updated,
    NEList<ottr:IRI> ?authors,
    ? NEList<ottr:IRI> ?contributors
]
#annotations
:: {
    ottr:Triple(?resource, pav:createdOn, ?created),
    ottr:Triple(?resource, pav:lastUpdateOn, ?updated),
    cross | ottr:Triple(?resource, dc:creator, ++?authors),
    cross | ottr:Triple(?resource, dc:contributor, ++?contributors)
} .
#+end_src

#+name: annotations-Provenance
 - Signature
   1. ?label =? string=  :: 
   2. ?description =! string=  :: "Provenance data for templates. The
      '?created' parameter should denote the time of creation of the
      template and should be updated only for new major or minor
      versions of the template. Timestamps for patch updates are
      indicated with the '?updated' parameter." 
   3. ?scope =?! string=  :: 
   4. ?notes =? NEList<string>=  :: "The template should be used in
      combination with other templates in the same package."
   5. ?seeAlso =? NEList<IRI>=  :: o-docttr:
   6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
   1. ?created =! dateTime=  :: 2020-08-21T00:00:00Z
   2. ?updated =?! dateTime=  :: 
   3. ?authors =NEList<IRI>=  :: <http://folk.uio.no/martige/foaf.rdf#me>
   4. ?contributors =? NEList<IRI>=  :: 
 - Version
   1. ?status =! IRI= :: ottr:draft
   2. ?version =! string= :: "0.1.1"
   3. ?previousVersion =!? IRI= :: 
   4. ?nextVersion =!? IRI= :: 

* docTTR HTML pages                                                :noexport:
# Execute this call with C-c C-c to generate docTTR HTML pages:
#+call: write-docttr()

#+RESULTS:
#+begin_example
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Signature to docttr\tpl.ottr.xyz\p\docttr\0.1\Signature.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Deprecated to docttr\tpl.ottr.xyz\p\docttr\0.1\Deprecated.html
Wrote http://tpl.ottr.xyz/rdf/0.1/Statement to docttr\tpl.ottr.xyz\rdf\0.1\Statement.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Provenance to docttr\tpl.ottr.xyz\p\docttr\0.1\Provenance.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Example to docttr\tpl.ottr.xyz\p\docttr\0.1\Example.html
Wrote http://tpl.ottr.xyz/rdf/0.1/Type to docttr\tpl.ottr.xyz\rdf\0.1\Type.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/SaysWhoTriple to docttr\tpl.ottr.xyz\p\docttr\0.1\SaysWhoTriple.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Version to docttr\tpl.ottr.xyz\p\docttr\0.1\Version.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/SaysWho to docttr\tpl.ottr.xyz\p\docttr\0.1\SaysWho.html
Wrote http://tpl.ottr.xyz/rdf/0.1/StatementTriple to docttr\tpl.ottr.xyz\rdf\0.1\StatementTriple.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/ChangeNote to docttr\tpl.ottr.xyz\p\docttr\0.1\ChangeNote.html
Wrote http://tpl.ottr.xyz/p/docttr/0.1/Parameter to docttr\tpl.ottr.xyz\p\docttr\0.1\Parameter.html
Wrote index files to docttr
Wrote index files to docttr\tpl.ottr.xyz
Wrote index files to docttr\tpl.ottr.xyz\p
Wrote index files to docttr\tpl.ottr.xyz\p\docttr
Wrote index files to docttr\tpl.ottr.xyz\p\docttr\0.1
Wrote index files to docttr\tpl.ottr.xyz\rdf
Wrote index files to docttr\tpl.ottr.xyz\rdf\0.1
#+end_example

* Startup for this file                                            :noexport:
#+STARTUP: hideblocks
# Local Variables:
# mode: org
# org-confirm-babel-evaluate: nil
# org-babel-default-inline-header-args: ((:exports . "code"))
# org-latex-listings: t
# bottr-dir: "./bottr"
# docttr-dir: "./docttr"
# data-dir: "./data"
# eval: (org-babel-load-file (concat emacs-ottr-toolkit-root "ottr-extra.org"))
# eval: (org-babel-lob-ingest (concat emacs-ottr-toolkit-root "ottr-lob.org"))
# eval: (setq-local org-link-abbrev-alist
# (quote
#  (
# ( "owl" . "http://www.w3.org/2002/07/owl#")
# ( "rdf" . "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
# ( "xml" . "http://www.w3.org/XML/1998/namespace")
# ( "xsd" . "http://www.w3.org/2001/XMLSchema#")
# ( "rdfs" . "http://www.w3.org/2000/01/rdf-schema#")
# ( "dc" . "http://purl.org/dc/elements/1.1/")
# ( "skos" . "http://www.w3.org/2004/02/skos/core#")
# ( "foaf" . "http://xmlns.com/foaf/0.1/")
# ( "ottr" . "http://ns.ottr.xyz/0.4/")
# ( "o-rdf" . "http://tpl.ottr.xyz/rdf/0.1/")
# ( "o-rdfs" . "http://tpl.ottr.xyz/rdfs/0.1/")
# ( "o-owl-ax" . "http://tpl.ottr.xyz/owl/axiom/0.1/")
# ( "o-docttr" . "http://tpl.ottr.xyz/p/docttr/0.1/")
# )))
# org-todo-keyword-faces: (("REVIEW" . "orange") ("REDO" . "orange") ("N/A" . "darkgreen"))
# org-log-into-drawer: t
# eval: (setq-local org-babel-default-header-args:stottr
# '((:comments . "link")
#   ))
# End:
