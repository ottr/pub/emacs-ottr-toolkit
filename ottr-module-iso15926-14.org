#+title: OTTR template module for ISO 15926-14
#+author: Johan W. Klüwer
#+email: johan.wilhelm.kluewer@dnv.com
#+date: started 2021-06-10
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup

* Tasks
** TODO Change namespaces, from ISO/DNV to PCA
For resources in Part 14:
 - from http://standards.iso.org/iso/15926/part14/
 - to http://rds.posccaesar.org/ontology/lis14/rdl/
 
For this template library:
 - from http://data.dnv.com/ontology/iso15926-14/tpl/
 - to http://rds.posccaesar.org/ontology/lis14/tpl/
** TODO Publish on rds.posccaesar.org
 - each template at a non-versioned URI, e.g. http://rds.posccaesar.org/ontology/iso15926-14/tpl/LT_0001.stottr
 - and at a versioned
   uri. http://rds.posccaesar.org/ontology/iso15926-14/tpl/0.1/LT_0001.stottr
 - check that we get the same facilities as with the core OTTR
   library: provide HTML, stottr, Turtle formats.
** TODO Consider which patterns really qualify for inclusion
Q. Should we require every template, or every /public/ template, in this
library to make use of at least one resource from Part 14? Templates
that only use OWL itself will not have any dependency on Part 14, and
should probably be located elsewhere.
* What is this
This file defines templates for creating OWL ontologies using ISO
15926-14 as upper ontology. 

The format of this file follows the [[https://gitlab.com/ottr/pub/emacs-ottr-toolkit][emacs-ottr-toolkit]].

* Template definitions
:PROPERTIES:
:header-args:stottr:  :noweb yes :tangle ./stottr/LIS-tpl.stottr
:header-args:ttl:  :noweb yes
:header-args: :padline yes
:END:

** Prefixes
#+name: stottr-prefixes
#+begin_src stottr :noweb yes
  <<ttl-prefixes>>
  @prefix o-rdf: <http://tpl.ottr.xyz/rdf/0.1/> .
  @prefix o-rdfs: <http://tpl.ottr.xyz/rdfs/0.2/> .
  @prefix o-owl-ax: <http://tpl.ottr.xyz/owl/axiom/0.1/> .
  @prefix o-owl-dec: <http://tpl.ottr.xyz/owl/declaration/0.1/>.
  @prefix o-owl-ma:   <http://tpl.ottr.xyz/owl/macro/0.1/>.
  @prefix rstr: <http://tpl.ottr.xyz/owl/restriction/0.1/>.
  @prefix o-docttr: <http://tpl.ottr.xyz/p/docttr/0.1/> .

  @prefix lis: <http://rds.posccaesar.org/ontology/lis14/rdl/> .
  @prefix listpl: <http://rds.posccaesar.org/ontology/iso15926-14/tpl/> .
  @prefix ex: <http://example.org/> .
#+end_src

** Private templates

*** Quantities and quantified values
**** listpl:LT_0001 quantity datum with type
#+name: LT_0001
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0001" "annotations-LT_0001")
  listpl:LT_0001 [
  ! ottr:IRI ?quantityDatum, ?! owl:Class ?datumType, ! owl:NamedIndividual ?uom, ! xsd:double ?value ]
  #annotations
  :: {
  o-rdf:Type ( ?quantityDatum, ?datumType ),
  ottr:Triple ( ?quantityDatum, lis:datumUOM, ?uom ),
  ottr:Triple ( ?quantityDatum, lis:datumValue, ?value )
  } .
#+end_src

#+name: annotations-LT_0001
 - Signature
     1. ?label =? string=  :: "Quantity Datum with type, UOM, value"
     2. ?description =! string= :: "?quantityDatum is a datum (intended:
        ScalarQuantityDatum) of optional type ?datumType, with unit of
        measure ?uom, and numeric value ?value."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T15:06:40Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0001-data-table
#+call: template-data-table( template="LT_0001" ) :cache yes

#+RESULTS[c297750db9d58c4abadc43987b144483dc88cba6]: LT_0001-data-table
| quantityDatum | datumType | uom   | value            |
|---------------+-----------+-------+------------------|
| ex:aDatum     | ex:Length | ex:Cm | "10"^^xsd:double |
| ex:bDatum     | ex:Width  | ex:Cm | 67               |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0001-data-table, file="LT_0001-data-table.csv", template="LT_0001" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0001_LT_0001-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select quantityDatum, datumType, uom, value
from csvread( './data/LT_0001-data-table.csv' )
""" ;
ottr:template listpl:LT_0001 ;
ottr:argumentMaps (
  [ ottr:type ottr:IRI ] #  ?quantityDatum (no type)
  [ ottr:type owl:Class ] #  ?datumType optional non-blank default = lis:ScalarQuantityDatum
  [ ottr:type owl:NamedIndividual ] #  ?uom non-blank
  [ ottr:type xsd:double ] #  ?value non-blank
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0001-data-table, file="LT_0001-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0001_LT_0001-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix lis:   <http://standards.iso.org/iso/15926/part14/> .

ex:aDatum  a            ex:Length ;
        lis:datumUOM    ex:Cm ;
        lis:datumValue  "10"^^<http://www.w3.org/2001/XMLSchema#double> .

ex:bDatum  a            ex:Width ;
        lis:datumUOM    ex:Cm ;
        lis:datumValue  "67"^^<http://www.w3.org/2001/XMLSchema#double> .

#+end_SRC
**** listpl:LT_0002 quantity with type, and quantity datum with type
#+name: LT_0002
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0002" "annotations-LT_0002")
  listpl:LT_0002 [ ottr:IRI ?quantity,
  ?! owl:Class ?quantityType,
  ! owl:ObjectProperty ?quantifiedAs = lis:qualityQuantifiedAs,
  ! ottr:IRI ?quantityDatum = _:qd,
  ?! owl:Class ?datumType,
  ! owl:NamedIndividual ?uom,
  ! xsd:double ?value
  ] 
  #annotations
  :: {
  ottr:Triple ( ?quantity, ?quantifiedAs, ?quantityDatum ),
  o-rdf:Type ( ?quantity, ?quantityType ),
  listpl:LT_0001 ( ?quantityDatum, ?datumType, ?uom, ?value )
  } .
#+end_src

#+name: annotations-LT_0002
 - Signature
     1. ?label =? string=  :: "Physical Object has Quantity Value"
     2. ?description =! string=  :: "The ?quantity has a ... ."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T14:33:39Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0002-data-table
#+call: template-data-table( template="LT_0002" ) :cache yes

#+RESULTS[52b309ee9e067ebda0eb1ccf02ff256f8f3c781b]: LT_0002-data-table
| quantity        | quantityType | quantifiedAs  | quantityDatum        | datumType  | uom   | value |
|-----------------+--------------+---------------+----------------------+------------+-------+-------|
| ex:hammerMass   | ex:Mass      | ex:measuredAs | ex:WeightMeasurement | ex:myDatum | ex:Kg |     5 |
| ex:hammerLength |              |               |                      |            | ex:Cm |    45 |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0002-data-table, file="LT_0002-data-table.csv", template="LT_0002" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0002_LT_0002-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select quantity, quantityType, quantifiedAs, datumType, quantityDatum, uom, value
from csvread( './data/LT_0002-data-table.csv' )
""" ;
ottr:template listpl:LT_0002 ;
ottr:argumentMaps (
  [ ottr:type ottr:IRI ] #  ?quantity
  [ ottr:type owl:Class ] #  ?quantityType
  [ ottr:type owl:ObjectProperty ] #  ?quantifiedAs default = lis:qualityQuantifiedAs
  [ ottr:type owl:Class ] #  ?datumType optional
  [ ottr:type ottr:IRI ] #  ?quantityDatum
  [ ottr:type owl:NamedIndividual ] #  ?uom
  [ ottr:type xsd:double ] #  ?value
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0002-data-table, file="LT_0002-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0002_LT_0002-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix lis:   <http://standards.iso.org/iso/15926/part14/> .

ex:hammerLength  lis:qualityQuantifiedAs
                [ lis:datumUOM    ex:Cm ;
                  lis:datumValue  "45"^^<http://www.w3.org/2001/XMLSchema#double>
                ] .

ex:myDatum  a           ex:WeightMeasurement ;
        lis:datumUOM    ex:Kg ;
        lis:datumValue  "5"^^<http://www.w3.org/2001/XMLSchema#double> .

ex:hammerMass  ex:measuredAs  ex:myDatum .

#+end_SRC
**** listpl:LT_0006
This template is a copy of =ntpl:RestrictionMinMaxInclusiveExclusive=
from the Z-TI project (READI).
#+name: LT_0006
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0006" "annotations-LT_0006")
  listpl:LT_0006 [ ottr:IRI ?restriction,
  ottr:IRI ?xsdDatatype = xsd:double, 
  rdfs:Literal ?minimum,
  rdfs:Literal ?maximum
  ]
  #annotations
  :: {
  ottr:Triple(?restriction, owl:onDatatype, ?xsdDatatype),
  ottr:Triple(?restriction, owl:withRestrictions, (_:min, _:max)),
  ottr:Triple(?restriction, rdf:type, rdfs:Datatype),
  ottr:Triple(_:min, xsd:minInclusive, ?minimum),
  ottr:Triple(_:max, xsd:maxExclusive, ?maximum)
  } .
#+end_src

#+name: annotations-LT_0006
 - Signature
     1. ?label =? string=  :: 
     2. ?description =! string=  :: "create a datatype value range restriction"
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T21:45:07Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

** Public templates
*** listpl:LT_0003 physical object with quantity, quantified with uom and value
This template creates individuals with the "full" model of quantified physical quantities.
#+name: LT_0003
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0003" "annotations-LT_0003")
  listpl:LT_0003 [ ! owl:NamedIndividual ?physicalObject,
  ! owl:ObjectProperty ?hasQuantity = lis:hasPhysicalQuantity,
  ottr:IRI ?quantity = _:quantity,
  ?! owl:Class ?quantityType,
  ! owl:ObjectProperty ?quantifiedAs = lis:qualityQuantifiedAs,
  ! ottr:IRI ?quantityDatum = _:quantityDatum,
  ?! owl:Class ?datumType,
  ! owl:NamedIndividual ?uom,
  ! xsd:double ?value
  ]
  #annotations
  :: {
  ottr:Triple ( ?physicalObject, ?hasQuantity, ?quantity ),
  listpl:LT_0002 ( ?quantity, ?quantityType, ?quantifiedAs, ?quantityDatum, ?datumType, ?uom, ?value )
  } .
#+end_src

#+name: annotations-LT_0003
 - Signature
     1. ?label =? string=  :: "Object with quantified quantity"
     2. ?description =! string=  :: "physical object with quantity, quantified with uom and value"
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T19:40:43Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 

# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0003-data-table
#+call: template-data-table( template="LT_0003" ) :cache yes

#+RESULTS[c9e18576ccabe1fcbb3c51926c3d9ae4446a4cde]: LT_0003-data-table
| physicalObject | hasQuantity | quantity   | quantityType | quantifiedAs | quantityDatum     | datumType             | uom         | value |
|----------------+-------------+------------+--------------+--------------+-------------------+-----------------------+-------------+-------|
| ex:myHammer    | ex:hasMass  | ex:myHmass | ex:Mass      | ex:measured  | ex:myHweighResult | ex:MassDatum          | ex:Kg       |     3 |
| ex:myComb      |             |            |              |              |                   |                       | ex:Cm       |    15 |
| ex:myCat       | ex:hasLife  |            | ex:Life      | ex:counted   |                   | ex:CountDatum         | ex:Count    |     9 |
| ex:myEBike     | ex:hasPower |            |              | ex:estimated |                   | ex:PowerCapacityDatum | ex:Kilowatt |    .5 |
| ex:myFloor     | ex:hasArea  |            | ex:Area      | ex:deemed    |                   | ex:AreaDatum          | ex:Msquared |    20 |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0003-data-table, file="LT_0003-data-table.csv", template="LT_0003" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0003_LT_0003-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select physicalObject, hasQuantity, quantity, quantityType, quantifiedAs, quantityDatum, datumType, uom, value
from csvread( './data/LT_0003-data-table.csv' )
""" ;
ottr:template listpl:LT_0003 ;
ottr:argumentMaps (
  [ ottr:type owl:NamedIndividual ] #  ?physicalObject non-blank
  [ ottr:type owl:ObjectProperty ] #  ?hasQuantity non-blank default = lis:hasPhysicalQuantity
  [ ottr:type ottr:IRI ] #  ?quantity default = _:quantity
  [ ottr:type owl:Class ] #  ?quantityType optional non-blank
  [ ottr:type owl:ObjectProperty ] #  ?quantifiedAs non-blank default = lis:qualityQuantifiedAs
  [ ottr:type ottr:IRI ] #  ?quantityDatum non-blank default = _:quantityDatum
  [ ottr:type owl:Class ] #  ?datumType optional non-blank
  [ ottr:type owl:NamedIndividual ] #  ?uom non-blank
  [ ottr:type xsd:double ] #  ?value non-blank
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0003-data-table, file="LT_0003-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0003_LT_0003-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix lis:   <http://standards.iso.org/iso/15926/part14/> .

ex:myEBike  ex:hasPower  [ ex:estimated  [ a               ex:PowerCapacityDatum ;
                                           lis:datumUOM    ex:Kilowatt ;
                                           lis:datumValue  "0.5"^^<http://www.w3.org/2001/XMLSchema#double>
                                         ]
                         ] .

ex:myHmass  a        ex:Mass ;
        ex:measured  ex:myHweighResult .

ex:myCat  ex:hasLife  [ a           ex:Life ;
                        ex:counted  [ a               ex:CountDatum ;
                                      lis:datumUOM    ex:Count ;
                                      lis:datumValue  "9"^^<http://www.w3.org/2001/XMLSchema#double>
                                    ]
                      ] .

ex:myHammer  ex:hasMass  ex:myHmass .

ex:myFloor  ex:hasArea  [ a          ex:Area ;
                          ex:deemed  [ a               ex:AreaDatum ;
                                       lis:datumUOM    ex:Msquared ;
                                       lis:datumValue  "20"^^<http://www.w3.org/2001/XMLSchema#double>
                                     ]
                        ] .

ex:myComb  lis:hasPhysicalQuantity  [ lis:qualityQuantifiedAs  [ lis:datumUOM    ex:Cm ;
                                                                 lis:datumValue  "15"^^<http://www.w3.org/2001/XMLSchema#double>
                                                               ]
                                    ] .

ex:myHweighResult  a    ex:MassDatum ;
        lis:datumUOM    ex:Kg ;
        lis:datumValue  "3"^^<http://www.w3.org/2001/XMLSchema#double> .

#+end_SRC

*** listpl:LT_0009 physical object with 'shortcut' data property value
This template declares a named individual, and asserts a datatype
property fact with =xsd:double= value.
#+name: LT_0009
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0009" "annotations-LT_0009")
    listpl:LT_0009 [ ! owl:NamedIndividual ?physicalObject,
      ! owl:DatatypeProperty ?shortcutDataProp,
      ! xsd:double ?value]
    #annotations
    :: {
    ottr:Triple( ?physicalObject, ?shortcutDataProp, ?value )
    } .
#+end_src

#+name: annotations-LT_0009
 - Signature
     1. ?label =? string=  :: 
     2. ?description =! string=  :: "Physical object with 'shortcut'
        data property value."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-22T13:23:29Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= ::

# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0009-data-table
#+call: template-data-table( template="LT_0009" ) :cache yes

Using the value "-5", without decimals, to test whether this will be
interpreted correctly.
#+RESULTS[8906e624018224795e9bd78b3c24b9d0d6269e7e]: LT_0009-data-table
| physicalObject | shortcutDataProp | value |
|----------------+------------------+-------|
| ex:ind1        | ex:r             |  10.0 |
| ex:ind2        | ex:r             |    -5 |
# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0009-data-table, file="LT_0009-data-table.csv", template="LT_0009" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0009_LT_0009-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select physicalObject, shortcutDataProp, value
from csvread( './data/LT_0009-data-table.csv' )
""" ;
ottr:template listpl:LT_0009 ;
ottr:argumentMaps (
  [ ottr:type owl:NamedIndividual ] #  ?physicalObject non-blank
  [ ottr:type owl:DatatypeProperty ] #  ?shortcutDataProp non-blank
  [ ottr:type xsd:double ] #  ?value non-blank
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0009-data-table, file="LT_0009-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0009_LT_0009-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .

ex:ind2  ex:r   "-5"^^<http://www.w3.org/2001/XMLSchema#double> .

ex:ind1  ex:r   "10.0"^^<http://www.w3.org/2001/XMLSchema#double> .

#+end_SRC

*** listpl:LT_0008 datatype property with domain, range, and superproperty
#+name: LT_0008
# #+header: :tangle no
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0008" "annotations-LT_0008")
  listpl:LT_0008 [ ! owl:DatatypeProperty ?dataProperty,
    xsd:string ?label,
    ? xsd:string ?comment,
    ? List<rdfs:Resource> ?seeAlso,
    ? ottr:IRI ?definedBy,
    !? owl:Class ?domain,
    !? ottr:IRI ?range,
    ? owl:DatatypeProperty ?superProperty ]
  #annotations
  :: {
  o-owl-dec:DatatypeProperty( ?dataProperty, ?label, ?comment, ?seeAlso, ?definedBy ),
  o-owl-ax:SubDataPropertyOf ( ?dataProperty, ?superProperty ),
  o-rdfs:Domain ( ?dataProperty, ?domain ),
  o-rdfs:Range ( ?dataProperty, ?range )
  } .
#+end_src

#+name: annotations-LT_0008
 - Signature
     1. ?label =? string=  :: 
     2. ?description =! string=  :: "Introduce a data property with
        domain, range, and superproperty."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-22T11:33:34Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 
# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0008-data-table
#+call: template-data-table( template="LT_0008" ) :cache yes

#+RESULTS[fd6edbb472152afbb1bf13caf8f4379f20fb38e4]: LT_0008-data-table
| dataProperty  | label        | comment                    | seeAlso | definedBy | domain | range | superProperty |
|---------------+--------------+----------------------------+---------+-----------+--------+-------+---------------|
| ex:myDataProp | my data prop | my comment to my data prop |         |           | ex:C   |       | ex:rSuper     |
# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0008-data-table, file="LT_0008-data-table.csv", template="LT_0008" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0008_LT_0008-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select dataProperty, label, comment, seeAlso, definedBy, domain, range, superProperty
from csvread( './data/LT_0008-data-table.csv' )
""" ;
ottr:template listpl:LT_0008 ;
ottr:argumentMaps (
  [ ottr:type owl:DatatypeProperty ] #  ?dataProperty non-blank
  [ ottr:type xsd:string ] #  ?label
  [ ottr:type xsd:string ] #  ?comment optional
  [ ottr:type ( rdf:List ottr:IRI )  ] #  ?seeAlso optional
  [ ottr:type ottr:IRI ] #  ?definedBy optional
  [ ottr:type owl:Class ] #  ?domain optional non-blank
  [ ottr:type ottr:IRI ] #  ?range optional non-blank
  [ ottr:type owl:DatatypeProperty ] #  ?superProperty optional
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0008-data-table, file="LT_0008-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0008_LT_0008-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

ex:myDataProp  a            owl:DatatypeProperty ;
        rdfs:comment        "my comment to my data prop" ;
        rdfs:domain         ex:C ;
        rdfs:label          "my data prop" ;
        rdfs:subPropertyOf  ex:rSuper .

ex:rSuper  a    owl:DatatypeProperty .

#+end_SRC

*** listpl:LT_0004 introduce a "shortcut" data property
This template makes =rdfs:seeAlso= pointers to the physical quantity,
means of quantification, and unit of measure. This should be improved
to use three separate annotation properties ("defined by physical
quantity", etc.). However, this can't happen until those annotation
properties are added to LIS-14 itself.
#+name: LT_0004
# #+header: :tangle no
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0004" "annotations-LT_0004")
  listpl:LT_0004 [ ! owl:DatatypeProperty ?shortcutProperty,
  xsd:string ?label,
  ? xsd:string ?comment,
  !? owl:Class ?domain,
  !? ottr:IRI ?range,
  owl:DatatypeProperty ?superProperty = lis:qualityQuantityValue,
  ? owl:Class ?PhysicalQuantity,
  ? owl:ObjectProperty ?howQuantified,
  ? owl:NamedIndividual ?uom
  ]
  #annotations
  :: {
  listpl:LT_0008 ( ?shortcutProperty, ?label, ?comment, (?PhysicalQuantity, ?howQuantified, ?uom), ottr:none, ?domain, ?range, ?superProperty )
  } .
#+end_src

#+name: annotations-LT_0004
 - Signature
     1. ?label =? string=  :: 
     2. ?description =! string=  :: "Template for introducing 'shortcut'
        data property, combining the physical quantity (e.g., mass),
        the way of quantification (e.g., measured as, or stipulated
        as), and the unit of measure (e.g., Cm)."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T20:55:07Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 
# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0004-data-table
#+call: template-data-table( template="LT_0004" ) :cache yes

#+RESULTS[da077eeed1c3d07ba30ff955303ddbb05116d9ed]: LT_0004-data-table
| shortcutProperty    | label             | comment                    | domain             | range      | superProperty | PhysicalQuantity | howQuantified | uom   |
|---------------------+-------------------+----------------------------+--------------------+------------+---------------+------------------+---------------+-------|
| ex:mass_measured_kg | mass, measured kg | A "shortcut" data property | lis:PhysicalObject | xsd:double |               | ex:Mass          | ex:measured   | ex:kg |

# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0004-data-table, file="LT_0004-data-table.csv", template="LT_0004" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0004_LT_0004-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select shortcutProperty, label, comment, domain, range, superProperty, PhysicalQuantity, howQuantified, uom
from csvread( './data/LT_0004-data-table.csv' )
""" ;
ottr:template listpl:LT_0004 ;
ottr:argumentMaps (
  [ ottr:type owl:DatatypeProperty ] #  ?shortcutProperty non-blank
  [ ottr:type xsd:string ] #  ?label
  [ ottr:type xsd:string ] #  ?comment optional
  [ ottr:type owl:Class ] #  ?domain optional non-blank
  [ ottr:type ottr:IRI ] #  ?range optional non-blank
  [ ottr:type owl:DatatypeProperty ] #  ?superProperty default = lis:qualityQuantityValue
  [ ottr:type owl:Class ] #  ?PhysicalQuantity optional
  [ ottr:type owl:ObjectProperty ] #  ?howQuantified optional
  [ ottr:type owl:NamedIndividual ] #  ?uom optional
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0004-data-table, file="LT_0004-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0004_LT_0004-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix lis:   <http://standards.iso.org/iso/15926/part14/> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

lis:qualityQuantityValue
        a       owl:DatatypeProperty .

ex:mass_measured_kg  a      owl:DatatypeProperty ;
        rdfs:comment        "A \"shortcut\" data property" ;
        rdfs:domain         lis:PhysicalObject ;
        rdfs:label          "mass, measured kg" ;
        rdfs:range          xsd:double ;
        rdfs:seeAlso        ex:measured , ex:Mass , ex:kg ;
        rdfs:subPropertyOf  lis:qualityQuantityValue .

#+end_SRC

*** listpl:LT_0007 class restriction on datatype property: values min--max
This template is a copy of =ntpl:SubDataMinMaxInclusiveExclusiveDouble=.
#+name: LT_0007
#+begin_src stottr :var annotations=(ottr-annotations-str "listpl:LT_0007" "annotations-LT_0007")
  listpl:LT_0007 [ owl:Class ?class,
  owl:DatatypeProperty ?dataProp,
  rdfs:Literal ?minimum = "-INF"^^xsd:double,
  rdfs:Literal ?maximum = "INF"^^xsd:double ]
  #annotations
  :: {
      listpl:LT_0006(_:rangeRestriction, xsd:double, ?minimum, ?maximum),
      o-owl-ax:SubDataAllValuesFrom(?class, ?dataProp, _:rangeRestriction)
  } .
#+end_src

#+name: annotations-LT_0007
 - Signature
     1. ?label =? string=  :: 
     2. ?description =! string=  :: "class restriction on datatype
        property to a minimal (inclusive) -- maximal (exclusive)
        xsd:double range."
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: 2021-06-10T21:53:53Z
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: <https://orcid.org/0000-0002-7167-7321>
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: "0.1.1"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 
# Execute this call with C-c C-c to insert blank table for template.
#+name: LT_0007-data-table
#+call: template-data-table( template="LT_0007" ) :cache yes

#+RESULTS[1088e4b884d0db562cc6b07cd71577f44a4bf6f0]: LT_0007-data-table
| class    | dataProp | minimum | maximum |
|----------+----------+---------+---------|
| ex:Human | ex:age   |       0 |     120 |
| ex:Human | ex:love  |       0 |         |
# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
#+call: bottr-test-map( table=LT_0007-data-table, file="LT_0007-data-table.csv", template="LT_0007" )

#+RESULTS:
#+begin_src ttl :tangle ./bottr/LT_0007_LT_0007-data-table.bottr
<<stottr-prefixes>>
[] a ottr:InstanceMap ; ottr:source [ a ottr:H2Source ] ; ottr:query """
select class, dataProp, minimum, maximum
from csvread( './data/LT_0007-data-table.csv' )
""" ;
ottr:template listpl:LT_0007 ;
ottr:argumentMaps (
  [ ottr:type owl:Class ] #  ?class
  [ ottr:type owl:DatatypeProperty ] #  ?dataProp
  [ ottr:type rdfs:Literal ] #  ?minimum
  [ ottr:type rdfs:Literal ] #  ?maximum
) .
#+end_src

# Execute this call to export the table of data to file:
#+call: csv-export-table( tbl=LT_0007-data-table, file="LT_0007-data-table.csv")

# Execute this call to run the bOTTR map:
#+call: expand-bottr(bottr="./bottr/LT_0007_LT_0007-data-table.bottr")

#+RESULTS:
#+begin_SRC ttl
@prefix ex:    <http://example.org/> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

ex:age  a       owl:DatatypeProperty .

ex:Human  rdfs:subClassOf  [ a                  owl:Restriction ;
                             owl:allValuesFrom  [ a                     rdfs:Datatype ;
                                                  owl:onDatatype        xsd:double ;
                                                  owl:withRestrictions  ( [ xsd:minInclusive  "0" ]
                                                                          [ xsd:maxExclusive  "INF"^^xsd:double ]
                                                                        )
                                                ] ;
                             owl:onProperty     ex:love
                           ] ;
        rdfs:subClassOf  [ a                  owl:Restriction ;
                           owl:allValuesFrom  [ a                     rdfs:Datatype ;
                                                owl:onDatatype        xsd:double ;
                                                owl:withRestrictions  ( [ xsd:minInclusive  "0" ]
                                                                        [ xsd:maxExclusive  "120" ]
                                                                      )
                                              ] ;
                           owl:onProperty     ex:age
                         ] .

ex:love  a      owl:DatatypeProperty .

#+end_SRC


* Library lint                                                     :noexport:
#+call: lint-stottr()

#+RESULTS:
#+begin_src org
No errors found.
#+end_src

* docTTR HTML pages                                                :noexport:
# Execute this call with C-c C-c to generate docTTR HTML pages:
#+call: write-docttr()

#+RESULTS:
#+begin_example
Wrote http://data.dnv.com/ontology/iso15926-14/tpl/LT_0003 to docttr\data.dnv.com\ontology\iso15926-14\tpl\LT_0003.html
Wrote http://data.dnv.com/ontology/iso15926-14/tpl/LT_0002 to docttr\data.dnv.com\ontology\iso15926-14\tpl\LT_0002.html
Wrote http://data.dnv.com/ontology/iso15926-14/tpl/LT_0001 to docttr\data.dnv.com\ontology\iso15926-14\tpl\LT_0001.html
Wrote http://tpl.ottr.xyz/rdf/0.1/Type to docttr\tpl.ottr.xyz\rdf\0.1\Type.html
Wrote index files to docttr
Wrote index files to docttr\tpl.ottr.xyz
Wrote index files to docttr\tpl.ottr.xyz\rdf
Wrote index files to docttr\tpl.ottr.xyz\rdf\0.1
Wrote index files to docttr\data.dnv.com
Wrote index files to docttr\data.dnv.com\ontology
Wrote index files to docttr\data.dnv.com\ontology\iso15926-14
Wrote index files to docttr\data.dnv.com\ontology\iso15926-14\tpl
#+end_example

* expand library, for web publishing
#+call: expand-library()

#+RESULTS:

* Startup for this file                                            :noexport:
# allow underscores in export, allow deep sections
#+OPTIONS: ^:{} H:6

#+STARTUP: hideblocks
# Local Variables:
# mode: org
# org-confirm-babel-evaluate: nil
# org-babel-default-inline-header-args: ((:exports . "code"))
# org-latex-listings: t
# stottr-dir: "./stottr"
# bottr-dir: "./bottr"
# docttr-dir: "./docttr"
# data-dir: "./data"
# eval: (org-babel-load-file (concat emacs-ottr-toolkit-root "ottr-extra.org"))
# eval: (org-babel-lob-ingest (concat emacs-ottr-toolkit-root "ottr-lob.org"))
# eval: (setq-local org-link-abbrev-alist
# (quote
#  (
# ( "owl" . "http://www.w3.org/2002/07/owl#")
# ( "rdf" . "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
# ( "xml" . "http://www.w3.org/XML/1998/namespace")
# ( "xsd" . "http://www.w3.org/2001/XMLSchema#")
# ( "rdfs" . "http://www.w3.org/2000/01/rdf-schema#")
# ( "dc" . "http://purl.org/dc/elements/1.1/")
# ( "skos" . "http://www.w3.org/2004/02/skos/core#")
# ( "foaf" . "http://xmlns.com/foaf/0.1/")
# ( "ottr" . "http://ns.ottr.xyz/0.4/")
# ( "o-rdf" . "http://tpl.ottr.xyz/rdf/0.1/")
# ( "o-rdfs" . "http://tpl.ottr.xyz/rdfs/0.1/")
# ( "o-owl-ax" . "http://tpl.ottr.xyz/owl/axiom/0.1/")
# ( "o-docttr" . "http://tpl.ottr.xyz/p/docttr/0.1/")
# )))
# org-todo-keyword-faces: (("REVIEW" . "orange") ("REDO" . "orange") ("N/A" . "darkgreen"))
# org-log-into-drawer: t
# eval: (setq-local org-babel-default-header-args:stottr
# '((:comments . "link")
#   ))
# End:
