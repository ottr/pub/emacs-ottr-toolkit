;;; ttl-imenu.el --- Build imenu for Turtle file -*- lexical-binding: t; -*-

(defconst ttlre-prefix "[-a-z_A-Z0-9]*" 
  "Pattern to match RDF prefix")
(defconst ttlre-elname "[a-z_A-Z0-9-.]+" 
  "Pattern to match RDF 'localname'")
(defconst ttlre-puri (concat ttlre-prefix ":" ttlre-elname) 
  "Pattern to match prefix-abbreviated URI")
(defconst ttlre-resource (concat "\\(" ttlre-puri "\\|<[^< \t\n]*>\\)" )
  "Pattern to match URIs, pURIs")
(defconst ttlre-resource-a  (concat "\\([\n\t ]+a[\n\t ]+\\|" ttlre-puri "\\|<[^<]*>\\)" )
  "Pattern to match URIs, pURIs, and the abbreviation 'a' for rdf:type")
(defconst ttlre-nlblank "[\n\t ]*" 
  "Pattern to match newlines or tabs or spaces")
(defconst ttlre-triple-end (concat ttlre-nlblank "[;.]") 
  "Pattern to match triple ending, with semicolon or dot.")
(defconst ttlre-lang-tag "@\\w\\w" 
  "Pattern to match string language tag, e.g. '@en'")
(defconst ttlre-datatype-tag "\\^\\^xsd:\\w+\\|\\^\\^<http://www.w3.org/2001/XMLSchema#\\w+>" 
  "Pattern to match xsd datatype tag, e.g. '^^xsd:string'")
(defconst ttlre-object-delim (concat ttlre-nlblank "," ttlre-nlblank) 
  "Pattern to match comma used to delimit multiple s-p-objects.")
(defconst ttlre-single-quoted-string "[\"].*[\"]" 
  "Pattern to match a simple double-quoted string")
(defconst ttlre-triple-quoted-string "\"\"\"[^\"]*\\(\\(\"[^\"]\\|\"\"[^\"]\\)[^\"]*\\)*\"\"\"" 
  "Pattern to match a triple-quoted string")
(defconst ttlre-quoted-string (concat "\\(" ttlre-single-quoted-string "\\|" ttlre-triple-quoted-string "\\)") 
  "Pattern to match any quoted string")
(defconst ttlre-anon-block "[[][\10-\377[:nonascii:]]*?[]]" 
  "Pattern to match an anonymous resource in square brackets")
(defconst ttlre-object (concat "\\("
                              ttlre-resource "\\|" 
                              ttlre-quoted-string 
                                "\\(" ttlre-lang-tag "\\)?" 
                                "\\(" ttlre-datatype-tag "\\)?" "\\|"
                              ttlre-anon-block 
                         "\\)" ) 
  "Pattern to match resources, strings, and anonymous blocks that can figure as RDF triple objects")

(defconst ttlre-block
  (concat "^" ttlre-resource
           ttlre-nlblank ttlre-resource-a
           ttlre-nlblank ttlre-object  "\\(" ttlre-object-delim ttlre-object "\\)*"
           "\\(" ttlre-nlblank ";"
                 ttlre-nlblank ttlre-resource-a
                 ttlre-nlblank ttlre-object  "\\(" ttlre-object-delim ttlre-object "\\)*" 
                "\\)*"
           ttlre-nlblank "[.]") "Pattern to match an RDF block with a subject and arbitrary predicate--object pairs.")

(defun ttlre-previous-block ()
 (interactive)
 (search-backward-regexp ttlre-block nil t))
(defun ttlre-next-block ()
 (interactive)
 (if (looking-at ttlre-block) (forward-char))
 (search-forward-regexp ttlre-block nil t)
 (goto-char (match-beginning 0)))

(defun ttlre-cut-next-block ()
 (interactive) 
 (progn (ttlre-next-block)
  (kill-region (match-beginning 0) (match-end 0))))

(defun ttlre-name-this-block () 
(save-excursion
  (if (not (looking-at ttlre-block)) (ttlre-previous-block))
    (if (search-forward-regexp "rdfs:label +\"\\(.*\\)\"" (match-end 0) t)
        (match-string 1)
      (progn (search-forward-regexp "[^ \t\n]+" nil t) (match-string 0)))))

(defun ttlre-type-name-pos-this-block () 
(save-excursion 
  (if (not (looking-at ttlre-block)) (ttlre-previous-block))
    (let* ((block-start (match-beginning 0)) (block-end (match-end 0))
     (block-name (ttlre-name-this-block))
    (block-type ; pick among "a" and "rdf:type" triples
     (if (search-forward-regexp "\\(a\\|rdf:type\\)" block-end t)
       (if (search-forward-regexp "owl:\\(Class\\|.*Property\\|.*Individual\\|Ontology\\)" block-end t) 
              (match-string 1)
           "other") "no type"))
        )
    (cons block-type (cons block-name block-start)))))

(defun ttlre-class-index ()
  (let ((index '()))
    (goto-char (point-max))
    (while (ttlre-previous-block)
      (push (ttlre-type-name-pos-this-block) index))
    (--map (cons (car it) (-map 'cdr (cdr it)))
      (--group-by (car it) index))))

(defconst stottr-blank "[[:space:]]*")
(defconst stottr-resource (concat "\\(" "\\(" ttlre-prefix "\\)" ":" "\\(" ttlre-elname "\\)" "\\|"
                                "<\\(http.+/\\)" "\\(" ttlre-elname "\\)>" "\\)"))
(defconst stottr-signature "\\([[][^]]+[]]\\)")
(defconst stottr-metadata "\\(@@.*([^{]*)\\)?")
(defconst stottr-arrow "[[:space:]]*::[[:space:]]*")
(defconst stottr-body "\\([{][^}]+[}]\\)")
(defconst stottr-closing "[[:space:]]*\\.")
(defconst stottr-block (concat "^" stottr-resource stottr-blank stottr-signature stottr-blank
  stottr-metadata stottr-arrow stottr-body stottr-closing))

(defun stottr-previous-block ()
 (interactive)
 (search-backward-regexp stottr-block nil t))
(defun stottr-next-block ()
 (interactive)
 (if (looking-at stottr-block) (forward-char))
 (search-forward-regexp stottr-block nil t)
 (goto-char (match-beginning 0)))

(defun stottr-ns-name-pos-this-block () 
  (save-excursion 
    (if (not (looking-at stottr-block)) (stottr-previous-block))
    (let* ((block-start (match-beginning 0)) (block-end (match-end 0))
           (block-name (substring-no-properties (or (match-string 3) (match-string 5))))  ; template localname
           (block-ns (substring-no-properties (or (match-string 2) (match-string 4))))  ; template namespace, prefixed or not
           (block-signature
(save-excursion (search-forward-regexp stottr-signature)
                (let* ((signature (substring-no-properties (match-string 0)))
                       (parameter-names
                        (if (s-match "?blank[0-9]" signature)
                            (s-match-strings-all " [^ \n!?]+"
                                                 (replace-regexp-in-string "\\?[a-zA-Z0-9,]+" "" signature))
                          (s-match-strings-all "\\?[^] ,\n]+" signature))))
                  (concat "[ "
                          (s-trim (s-join ", " (-flatten parameter-names)))
                          " ]")))
            ))
      (cons block-ns (cons (concat block-name block-signature) block-start)))))

(defun stottr-class-index ()
  (let ((index '()))
    (goto-char (point-max))
    (while (stottr-previous-block)
      (push (stottr-ns-name-pos-this-block) index))
    (let ((grouped ()))
      (dolist (entry index grouped)
        (let ((key (car entry))
              (value (cdr entry)))
          (push value (alist-get key grouped nil nil #'equal)))))))

(setq imenu-max-item-length 120)
