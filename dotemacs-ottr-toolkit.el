;; ----------------------------------------------------------------------
;; START Configuration for emacs-ottr-toolkit
;; ----------------------------------------------------------------------

; Add the contents of this file to your .emacs file.

;; ----------
;; USER INTERACTION SECTION: 
;; ----------

; 1. change this to point to where you have cloned the
;    emacs-ottr-toolkit directory (note, "~" points to the user home
;    directory)
(setq emacs-ottr-toolkit-root (expand-file-name "~/emacs-ottr-toolkit/"))

; the following assumes a directory "bin/" exists in the user home
; directory, and that it is on the PATH.

; 2. change this to point to the Lutra executable
(setq stottr-lutra-path (expand-file-name "~/bin/lutra.jar"))

; 3. create an executable shell script called
;    lutra-checkSyntax-stottr.bat (on Windows; accordingly on other OSes)
;    containing this command line:

;    java -jar %HOME%/bin/lutra.jar --mode checkSyntax -I stottr %*

; 4. If on Microsoft Windows: get a bash shell. One easy option is to
;    install Git: https://gitforwindows.org/
;    
;    Comment out the following lines if you are on Linux or Mac!
(setq explicit-shell-file-name "C:/Progra~1/Git/bin/bash.exe")
(setq explicit-bash-args '("--login" "-i"))
(setq shell-file-name explicit-shell-file-name)
(setq shell-command-switch "-c")

;; ----------


;; temporarily disable package signature checking, which will surely
;; trip up some new users
(setq package-check-signature nil)

;; see https://emacs.stackexchange.com/questions/54530/unattended-install-of-a-set-of-packages
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; bootstrap use-package, so I can use it to manage everything else
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

(use-package use-package
  :config
  (setq use-package-always-ensure t))

;; essentials for emacs
(use-package f :ensure t)
(use-package s :ensure t)
(use-package dash :ensure t)
(use-package ht :ensure t)
(require 'recentf)
(recentf-mode 1)
(use-package paren :ensure t)
(show-paren-mode 1)
(desktop-save-mode)
(desktop-read)
(prefer-coding-system 'utf-8)
(require 'org-tempo)

;; conveniences for emacs
(use-package smartscan :ensure t)

;; Turtle mode; use ttl-imenu.el
(use-package ttl-mode :ensure t)
(add-to-list 'auto-mode-alist '("\\.\\(n3\\|ttl\\|trig\\|bottr\\)" . ttl-mode))
(load-file (concat emacs-ottr-toolkit-root "ttl-imenu.el"))
(eval-after-load 'ttl
 '(progn
  (define-key ttl-mode-map "\C-\M-n" 'ttlre-next-block)
  (define-key ttl-mode-map "\C-\M-p" 'ttlre-previous-block)))
(add-hook 'ttl-mode-hook (lambda () (progn
  (setq imenu-create-index-function 'ttlre-class-index)
  (imenu-add-menubar-index))))

;; get stottr-mode.el
;; it will be installed in ~/elisp/stottr.el
;; and this will not work if stottr-mode.el moves somewhere else
(require 'url)
(unless (f-directory? "~/elisp") (f-mkdir "~/elisp"))
(unless (f-exists? "~/elisp/stottr-mode.el")
  (url-copy-file
   "https://gitlab.com/ottr/pub/emacs-stottr-mode/-/raw/master/stottr.el"
   "~/elisp/stottr-mode.el"))
(push (expand-file-name "~/elisp/") load-path)
(require 'stottr-mode)
(add-to-list 'auto-mode-alist '("\\.\\(stottr\\)" . stottr-mode))
(define-key stottr-mode-map "\C-\M-n" 'stottr-next-block)
(define-key stottr-mode-map "\C-\M-p" 'stottr-previous-block)
(add-hook 'stottr-mode-hook (lambda () (progn
  (setq imenu-create-index-function 'stottr-class-index)
  (imenu-add-menubar-index))))
(add-hook 'stottr-mode-hook (lambda () (smartscan-mode 1)))

; smart-compile for OTTR templates in stottr format
(require 'compile)
(use-package smart-compile :ensure t)
(setq stottr-lutra-checkSyntax
      (concat "java -jar " stottr-lutra-path " --mode checkSyntax -I stottr"))
(add-to-list 'smart-compile-alist `(stottr-mode . ,(concat stottr-lutra-checkSyntax " %f")))
(add-hook 'stottr-mode-hook (lambda () (define-key stottr-mode-map "\C-cc" 'smart-compile)))
(add-to-list 'compilation-error-regexp-alist 'stottr1)
(add-to-list 'compilation-error-regexp-alist 'stottr2)
(add-to-list 'compilation-error-regexp-alist-alist
             `(stottr1 ,(concat stottr-lutra-checkSyntax " \\(.*\\)")
                       1 nil nil 0)) ; file: INFO
(add-to-list 'compilation-error-regexp-alist-alist
             '(stottr2 "line \\([0-9]+\\) col \\([0-9]+\\)" nil 1 2)) ; line, col

; flycheck for stottr-mode
(use-package flycheck :ensure t)
; lutra-checkSyntax-stottr.bat is assumed to be on the path
(flycheck-define-checker stottr-syntax
  "A STOTTR syntax checker using Lutra.

See URL `https://www.ottr.xyz/\#Lutra'."
  :command ("lutra-checkSyntax-stottr.bat" source)
  :error-patterns
    ((error line-start "[ERROR] Syntax error at line " line " col " column ": " (message) line-end))
    :modes stottr-mode)
(add-to-list 'flycheck-checkers 'stottr-syntax)
(add-hook 'stottr-mode-hook 'flycheck-mode)

;; the yasnippet and yankpad packages for boilerplate insertion
; see http://www.howardism.org/Technical/Emacs/templates-tutorial.html
(use-package yasnippet
       :ensure t
       :init
       (yas-global-mode 1)
       :config
       (add-to-list 'yas-snippet-dirs (locate-user-emacs-file "snippets")))

; see https://github.com/Kungsgeten/yankpad
(use-package yankpad
  :ensure t
  :defer 10
  :init
  ;(setq yankpad-file (concat emacs-ottr-toolkit-root
                                        ;"yankpad-ottr.org"))
  (setq yankpad-file (concat emacs-ottr-toolkit-root "yankpad-ottr.org"))
  :config
  (bind-key "<f3>" 'yankpad-insert)
  ;; If you want to complete snippets using company-mode
  ;(add-to-list 'company-backends #'company-yankpad)
  ;; If you want to expand snippets with hippie-expand
  ;(add-to-list 'hippie-expand-try-functions-list #'yankpad-expand)
  )


;; org-babel
; Allow emacs-lisp evaluation with no questions asked. Clearly, this
; is a risky setting.
(defun my-org-confirm-babel-evaluate (lang body)
	(not (string= lang "emacs-lisp")))
(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

; ensure we can execute shell blocks
(eval-after-load 'org
    (org-babel-do-load-languages
     'org-babel-load-languages
     (append org-babel-load-languages
                              '((shell . t)))))

; load the emacs-ottr-toolkit helper functions
(org-babel-load-file (concat emacs-ottr-toolkit-root "ottr-extra.org"))
; load the emacs-ottr-toolkit org-babel blocks
(org-babel-lob-ingest (concat emacs-ottr-toolkit-root "ottr-lob.org"))

(setq package-check-signature t)
;; ----------------------------------------------------------------------
;; END Configuration for emacs-ottr-toolkit
;; ----------------------------------------------------------------------
