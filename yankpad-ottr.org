* org-mode
** Template sections
*** ottr-tpl-doc: OTTR template with docTTR annotations          :indent_nil:
\*** $1:$2
#+name: $2
#+begin_src stottr :var annotations=(ottr-annotations-str "$1:$2" "annotations-$2")
${1:prefix}:${2:TemplateName} [ $3 ]
#annotations
:: {
$4
} .
#+end_src

#+name: annotations-$2
 - Signature
     1. ?label =? string=  :: $5
     2. ?description =! string=  :: \"$6\"
     3. ?scope =?! string=  :: 
     4. ?notes =? NEList<string>=  :: 
     5. ?seeAlso =? NEList<IRI>=  :: 
     6. ?editorialNote =? NEList<string>=  :: 
 - Provenance
     1. ?created =! dateTime=  :: `(format-time-string "%Y-%m-%dT%H:%M:%SZ")`
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: $7
     4. ?contributors =? NEList<IRI>=  :: 
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: \"0.1.1\"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 
$0

*** ottr-tpl: OTTR template, no docTTR annotations               :indent_nil:
\*** $1:$2
#+name: $2
#+begin_src stottr
${1:prefix}:${2:TemplateName} [ $3 ]
:: {
$0
} .
#+end_src

** stottr: bare-bones stOTTR template source block               :indent_nil:
#+begin_src stottr
$1 [ $2  ]
:: {
$3
} .
#+end_src
$0
** data-table: blank table for filling in values to test template expansion
# Execute this call with C-c C-c to insert blank table for template.
#+name: $1-data-table
$0 #+call: template-data-table( template="${1:template name}" ) :cache yes
** bOTTR
*** bottr-map: bOTTR map for testing template with table of data
# Execute this call with C-c C-c, then tangle the resulting bottr block to file with C-u C-c C-v t.
$0 #+call: bottr-test-map( table=${1:table name}, file="$1.csv", template="${3:template name}" )
** docTTR description lists
*** docSignature: label, description, scope and editorial notes, related resources (docTTR)
 - Signature
     1. ?label =? string=  :: $1
     2. ?description =! string=  :: \"$2\"
     3. ?scope =?! string=  :: $3
     4. ?notes =? NEList<string>=  :: $4
     5. ?seeAlso =? NEList<IRI>=  :: $5
     6. ?editorialNote =? NEList<string>=  :: $6
$0
*** docProvenance: time of creation and update, authors and contributors (docTTR)
 - Provenance
     1. ?created =! dateTime=  :: `(format-time-string "%Y-%m-%dT%H:%M:%SZ")`
     2. ?updated =?! dateTime=  :: 
     3. ?authors =NEList<IRI>=  :: $1
     4. ?contributors =? NEList<IRI>=  :: $2
$0
*** docVersion: status, version number, references to previous and next versions (docTTR)
 - Version
     1. ?status =! IRI= :: ottr:draft
     2. ?version =! string= :: \"0.1.1\"
     3. ?previousVersion =!? IRI= :: 
     4. ?nextVersion =!? IRI= :: 
$0
*** docChangeNote: change description, including author and timestamp of the change (docTTR)
 - ChangeNote
     1. ?note =string= :: $1
     2. ?when =dateTime= :: `(format-time-string "%Y-%m-%dT%H:%M:%SZ")`
     3. ?who =NEList<Resource>= :: $2
     4. ?why =? List<string>= :: $3
$0
*** docExample: explanatory examples of the template (docTTR)
 - Example
     1. ?example =string= :: $1
     2. ?when =dateTime= :: `(format-time-string "%Y-%m-%dT%H:%M:%SZ")`
     3. ?who =NEList<Resource>= :: $2
     4. ?why =? List<string>= :: $3
$0
*** docDeprecated: mark the template as deprecated, including an explanation of why (docTTR)
 - Deprecated
     1. ?explanation =! string= :: \"$1\"
     2. ?seeAlso =? List<IRI>= :: $2
     3. ?when =! dateTime= :: `(format-time-string "%Y-%m-%dT%H:%M:%SZ")`
     4. ?who =NEList<Resource>= :: $3
     5. ?why =? List<string>= :: $4
$0

** ottr-doc: Document skeleton for OTTR package document         :indent_nil:
#+title: docTTR template package
#+author: `(if (s-blank? user-full-name) user-login-name user-full-name)`
#+email: `user-mail-address`
#+date: `(format-time-string "%Y-%m-%d")`
#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup

\* Template definitions
`""`:PROPERTIES:
:header-args:stottr:  :noweb yes :tangle ./stottr/$1.stottr
:header-args:ttl:  :noweb yes
:header-args: :padline yes
:END:
$0
\** Prefixes
#+name: stottr-prefixes
#+begin_src stottr :noweb yes
<<ttl-prefixes>>
@prefix o-rdf: <http://tpl.ottr.xyz/rdf/0.1/> .
@prefix o-rdfs: <http://tpl.ottr.xyz/rdfs/0.1/> .
@prefix o-owl-ax: <http://tpl.ottr.xyz/owl/axiom/0.1/> .
@prefix o-owl-dec: <http://tpl.ottr.xyz/owl/declaration/0.1/>.
@prefix rstr: <http://tpl.ottr.xyz/owl/restriction/0.1/>.
@prefix o-docttr: <http://tpl.ottr.xyz/p/docttr/0.1/> .
#+end_src

\** Private templates

\** Public templates

\* Library lint                                                     :noexport:
#+call: lint-stottr()

\* docTTR HTML pages                                                :noexport:
# Execute this call with C-c C-c to generate docTTR HTML pages:
#+call: write-docttr()

\* expand library, for web publishing                               :noexport:
#+call: expand-library()

\* Startup for this file                                            :noexport:
#+STARTUP: hideblocks
# Local Variables:
# mode: org
# org-confirm-babel-evaluate: nil
# org-babel-default-inline-header-args: ((:exports . "code"))
# org-latex-listings: t
# stottr-dir: "./stottr"
# bottr-dir: "./bottr"
# docttr-dir: "./docttr"
# data-dir: "./data"
# eval: (org-babel-load-file (concat emacs-ottr-toolkit-root "ottr-extra.org"))
# eval: (org-babel-lob-ingest (concat emacs-ottr-toolkit-root "ottr-lob.org"))
# eval: (org-babel-lob-ingest buffer-file-name)
# eval: (setq-local org-link-abbrev-alist
# (quote
#  (
# ( "owl" . "http://www.w3.org/2002/07/owl#")
# ( "rdf" . "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
# ( "xml" . "http://www.w3.org/XML/1998/namespace")
# ( "xsd" . "http://www.w3.org/2001/XMLSchema#")
# ( "rdfs" . "http://www.w3.org/2000/01/rdf-schema#")
# ( "dc" . "http://purl.org/dc/elements/1.1/")
# ( "skos" . "http://www.w3.org/2004/02/skos/core#")
# ( "foaf" . "http://xmlns.com/foaf/0.1/")
# ( "ottr" . "http://ns.ottr.xyz/0.4/")
# ( "o-rdf" . "http://tpl.ottr.xyz/rdf/0.1/")
# ( "o-rdfs" . "http://tpl.ottr.xyz/rdfs/0.1/")
# ( "o-owl-ax" . "http://tpl.ottr.xyz/owl/axiom/0.1/")
# ( "o-docttr" . "http://tpl.ottr.xyz/p/docttr/0.1/")
# )))
# org-todo-keyword-faces: (("REVIEW" . "orange") ("REDO" . "orange") ("N/A" . "darkgreen"))
# org-log-into-drawer: t
# eval: (setq-local org-babel-default-header-args:stottr
# '((:comments . "link")
#   ))
# End:

* stottr-mode
** tpl: OTTR template skeleton
